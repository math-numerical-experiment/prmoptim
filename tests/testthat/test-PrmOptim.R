test_that("Create vector without lock", {
    prm <- PrmOptim$new(list(a = list("PrmObjectVector", list(size = 3)),
                             b = list("PrmObjectVector", list(size = 5)),
                             c = list("PrmObjectVector", list(size = 1))),
                        list(g1 = list("a", "b"), g2 = list("c", "b")))
    prm$set_vector_list(list(a = c(-10, -20, -30), b = 1:5, c = 100))

    expect_that(prm$get_parameter_vector("g1"), equals(c(-10, -20, -30, 1, 2, 3, 4, 5)))
    expect_that(prm$get_parameter_vector("g2"), equals(c(100, 1, 2, 3, 4, 5)))
})

test_that("Create vector with lock", {
    prm <- PrmOptim$new(list(a = list("PrmObjectVector", list(size = 3)),
                             b = list("PrmObjectVector", list(size = 5)),
                             c = list("PrmObjectVector", list(size = 1))),
                        list(g1 = list("a", "b"), g2 = list("c", "b")))
    prm$set_vector_list(list(a = c(-10, -20, -30), b = 1:5, c = 100))
    prm$set_lock(list(a = c(TRUE, TRUE, FALSE), b = c(FALSE, FALSE, TRUE, TRUE, FALSE), c = TRUE))

    expect_that(prm$get_parameter_vector("g1"), equals(c(-30, 1, 2, 5)))
    expect_that(prm$get_parameter_vector("g2"), equals(c(1, 2, 5)))
})

test_that("Get values from parameters", {
    prm <- PrmOptim$new(list(a = list("PrmObjectVector", list(size = 3)),
                             b = list("PrmObjectVector", list(size = 5)),
                             c = list("PrmObjectVector", list(size = 1))),
                        list(g1 = list("a", "b"), g2 = list("c", "b")))
    prm$set_vector_list(list(a = c(-10, -20, -30), b = 1:5, c = 100))
    prm$set_lock(list(a = c(TRUE, TRUE, FALSE), b = c(FALSE, FALSE, TRUE, TRUE, FALSE), c = TRUE))

    prm_list1 <- prm$get_parameter_function("g1")
    prm_list2 <- prm$get_parameter_function("g2")
    expect_that(prm_list1$a(), equals(c(-10, -20, -30)))
    expect_that(prm_list1$b(), equals(1:5))
    expect_that(prm_list2$b(), equals(1:5))
    expect_that(prm_list2$c(), equals(100))
})

test_that("Get values from parameters", {
    prm <- PrmOptim$new(list(a = list("PrmObjectVector", list(size = 3)),
                             b = list("PrmObjectVector", list(size = 5)),
                             c = list("PrmObjectVector", list(size = 1))),
                        list(g1 = list("a", "b"), g2 = list("c", "b")))
    prm$set_vector_list(list(a = c(-10, -20, -30), b = 1:5, c = 100))
    prm$set_lock(list(a = c(TRUE, TRUE, FALSE), b = c(FALSE, FALSE, TRUE, TRUE, FALSE), c = TRUE))

    prm_list1 <- prm$get_parameter_function("g1")
    prm_list2 <- prm$get_parameter_function("g2")
    expect_that(prm_list1$a(), equals(c(-10, -20, -30)))
    expect_that(prm_list1$b(), equals(1:5))
    expect_that(prm_list2$b(), equals(1:5))
    expect_that(prm_list2$c(), equals(100))
})

test_that("Dump to list", {
    prm <- PrmOptim$new(list(a = list("PrmObjectVector", list(size = 3)),
                             b = list("PrmObjectVector", list(size = 5)),
                             c = list("PrmObjectVector", list(size = 1))),
                        list(g1 = list("a", "b"), g2 = list("c", "b")))
    prm$set_vector_list(list(a = c(-10, -20, -30), b = 1:5, c = 100))
    prm$set_lock(list(a = c(TRUE, TRUE, FALSE), b = c(FALSE, FALSE, TRUE, TRUE, FALSE), c = TRUE))

    dump <- prm$dump_to_list()
    expect_that(dump$parameter$a, equals(c(-10, -20, -30)))
    expect_that(dump$parameter$b, equals(1:5))
    expect_that(dump$parameter$c, equals(100))
    expect_that(dump$type$a, equals(list("PrmObjectVector", list(size = 3))))
    expect_that(dump$type$b, equals(list("PrmObjectVector", list(size = 5))))
    expect_that(dump$type$c, equals(list("PrmObjectVector", list(size = 1))))
    expect_that(dump$lock$a, equals(c(TRUE, TRUE, FALSE)))
    expect_that(dump$lock$b, equals(c(FALSE, FALSE, TRUE, TRUE, FALSE)))
    expect_that(dump$lock$c, equals(c(TRUE)))
    expect_that(dump$group$g1, equals(list("a", "b")))
    expect_that(dump$group$g2, equals(list("c", "b")))
})

test_that("Set vector", {
    prm <- PrmOptim$new(list(a = list("PrmObjectVector", list(size = 3)),
                             b = list("PrmObjectVector", list(size = 5)),
                             c = list("PrmObjectVector", list(size = 1))),
                        list(g1 = list("a", "b"), g2 = list("c", "b")),
                        list(a = c(-10, -20, -30), b = 1:5, c = 100),
                        list(b = c(FALSE, FALSE, TRUE, TRUE, FALSE), c = TRUE))

    prm$set_parameter_vector("g1", c(10, 20, 30, 40, 50, 60))

    expect_that(prm$get_parameter_vector("g1"), equals(c(10, 20, 30, 40, 50, 60)))
    prm_list1 <- prm$get_parameter_function("g1")
    expect_that(prm_list1$a(), equals(c(10, 20, 30)))
    expect_that(prm_list1$b(), equals(c(40, 50, 3, 4, 60)))
})
